CREATE TABLE review
(
	review_id INT NOT NULL AUTO_INCREMENT

	PRIMARY KEY (review_id)
) ENGINE=INNODB;


/* create schema as below
   echo('id= ' . $post->id);
   echo('<br>');
   echo('type= ' . $post->type);
   echo('<br>');
   echo('status_type= ' . $post->status_type);
   echo('<br>');
   echo('message= ' . $post->message);
   echo('<br>');
   echo('picture= ' . $post->picture);
   echo('<br>');
   echo('link= ' . $post->link);
   echo('<br>');
   echo('icon= ' . $post->icon);
   echo('<br>');
   echo('created_time= ' . $post->created_time);
   echo('<br>');
   echo('updated_time= ' . $post->updated_time);
   echo('<br>');

   if(isset($post->to) && $post->to != null)
   {
      foreach($post->to->data as $to)
      {
         echo('to id= ' . $to->id);
         echo('<br>');
         echo('to name= ' . $to->name);
         echo('<br>');
      }
   }
*/