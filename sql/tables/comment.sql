CREATE TABLE comment
(
	comment_id INT NOT NULL AUTO_INCREMENT,
	review_id INT NOT NULL,
	comment TEXT,

	PRIMARY KEY (comment_id),
	FOREIGN KEY (review_id) REFERENCES review (review_id)
) ENGINE=INNODB;

CREATE INDEX ix_comment_reviewid ON comment (review_id);
