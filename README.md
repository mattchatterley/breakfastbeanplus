# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this? ###

* A (mobile friendly) web search tool for http://www.facebook.com/TheBreakfastBean - which is a great page with reviews of breakfast joints in Jersey (CI) - but since Facebook pages are a bit cumbersome to search, this seemed like a fun idea - and a good way to do a bit more with the Graph API.

### Set up? ###

* Create database, put website somewhere it can be run from, run it. Instructions are a bit pointless as it doesn't currently work. It might by the time you read this if I forget to update this file, though.

### Who? ###

* By @mattchedit - matt.chatterley@gmail.com