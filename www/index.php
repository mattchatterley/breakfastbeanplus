<?php

require 'vendor/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

define('APP_ID', '327024427508799');
define('APP_SECRET', 'e41c9b6e1bf831573ec05b08e7df46e7');
define('APP_ACCESS_TOKEN', APP_ID . '|' . APP_SECRET);

FacebookSession::setDefaultApplication(APP_ID, APP_SECRET);

$session = new FacebookSession(APP_ACCESS_TOKEN);

// fetch all data in a paged fashion, so that if there are more posts in the future, we get them all
$data = array();
$paging = TRUE;

$pageSize = 2; // TODO: Set this to 250 for real data
$current = 0;

while($paging)
{
   $request = new FacebookRequest($session, 'GET', "/TheBreakfastBean/posts?limit=$pageSize&offset=$current");
   $response = $request->execute();

   $graphObject = $response->getGraphObject();

   $newData = $graphObject->getProperty('data')->asArray();
   $data = array_merge($data, $newData);

   $paging = count($newData) == $pageSize;
   $current += $pageSize;
}

// now parse posts into a format we can deal with
foreach($data as $post)
{
   // skip all 'stories' as these are not user generated content (they are things like 'xx liked yy')
   if(!empty($post->story))
   {
      continue;
   }

   //echo('<pre>');
   //var_dump($post);
   //echo('</pre>');

   echo('id= ' . $post->id);
   echo('<br>');
   echo('type= ' . $post->type);
   echo('<br>');
   echo('status_type= ' . $post->status_type);
   echo('<br>');
   echo('message= ' . $post->message);
   echo('<br>');
   echo('picture= ' . $post->picture);
   echo('<br>');
   echo('link= ' . $post->link);
   echo('<br>');
   echo('icon= ' . $post->icon);
   echo('<br>');
   echo('created_time= ' . $post->created_time);
   echo('<br>');
   echo('updated_time= ' . $post->updated_time);
   echo('<br>');

   if(isset($post->to) && $post->to != null)
   {
      foreach($post->to->data as $to)
      {
         echo('to id= ' . $to->id);
         echo('<br>');
         echo('to name= ' . $to->name);
         echo('<br>');
      }
   }

   foreach($post->comments->data as $comment)
   {
      if($comment->from->id == $post->from->id)
      {
         echo('comment= '. $comment->message);
         echo('<br>');
      }
   }

   echo('<br>');
   echo('<br>');
}

/*
{
   "id": "644448778907826",
   "about": "A review of Jersey breakfast places!\n",
   "can_post": false,
   "category": "Community",
   "checkins": 0,
   "cover": {
      "cover_id": 669133146439389,
      "offset_x": 0,
      "offset_y": 20,
      "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/s720x720/996800_669133146439389_1953345596_n.jpg?oh=2fdcbe286416d74ab2272c0125fdea58&oe=555419DB&__gda__=1432584486_cdb99f5ad721062bdc3a0d05050681f0",
      "id": "669133146439389"
   },
   "has_added_app": false,
   "is_community_page": false,
   "is_published": true,
   "likes": 3497,
   "link": "https://www.facebook.com/TheBreakfastBean",
   "name": "The Breakfast Bean",
   "parking": {
      "lot": 0,
      "street": 0,
      "valet": 0
   },
   "talking_about_count": 641,
   "username": "TheBreakfastBean",
   "website": "http://www.facebook.com/theburgerbean",
   "were_here_count": 0
}
*/
?>